import {createApp} from 'vue'
import App from '@/App.vue'
import vuetify from '@/plugins/vuetify'
import routes from '@/routes'
import stores from '@/store'
import {loadFonts} from '@/plugins/webfontloader'
import logOptions from '@/plugins/log'
import VueLogger from 'vuejs3-logger'
import axios from 'axios'
import VueAxios from 'vue-axios'

loadFonts()

createApp(App)
    .use(vuetify)
    .use(routes)
    .use(stores)
    .use(VueLogger, logOptions)
    .use(VueAxios, axios)
    .mount('#app')
