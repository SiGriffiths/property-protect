import { createRouter, createWebHistory } from 'vue-router'
import store from '@/store'

import Appointment from '@/routes/Appointment'
import Auth from '@/routes/Auth'
import Team from '@/routes/Team'

const routes = [
    ...Appointment,
    ...Auth,
    ...Team,
    {
        path: '/',
        name: 'dashboard',
        meta: {
            title: 'Dashboard'
        },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard')
    },
]

const router = createRouter({
    history: createWebHistory('/'),
    routes: routes,
})

router.beforeEach((to, from, next) => {
    const publicPages = ['/auth/login', '/auth/signup'];
    const authRequired = !publicPages.includes(to.path);
    if (authRequired && !store.getters['auth/isLoggedIn']) {
        next({name: 'auth-login'});
    } else {
        next();
    }

    store.dispatch('app/title', to.meta.title).then(() => {
        document.title = store.getters['app/title']
    })
});

export default router
