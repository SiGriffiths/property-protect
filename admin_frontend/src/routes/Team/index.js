export default [
    {
        path: '/team',
        name: 'team-list',
        meta: {
            title: 'Team'
        },
        component: () => import(/* webpackChunkName: "team" */ '@/views/Team/TeamList'),
    },
    {
        path: '/team/:id',
        name: 'team-detail',
        component: () => import(/* webpackChunkName: "team" */ '@/views/Team/TeamDetail'),
    },
    {
        path: '/team/:id/settings',
        name: 'team-settings',
        component: () => import(/* webpackChunkName: "team" */ '@/views/Team/TeamSettings'),
    }
]