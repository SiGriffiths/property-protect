export default {
    namespaced: true,
    state: () => ({
        organisations: [
            {
                id: '0d7ee1c5-262f-45da-95ce-91bccb38071a',
                name: "Super Org",
                orgAdmins: 1,
                teamAdmins: 4,
                reporters: 0,
                loneWorkers:27
            }
        ]
    }),
    getters: {
        organisations: (state) => {
            return state.organisations
        }
    },
    mutations: {
        // title: (state, payload) => {
        //     state.title = payload
        // },
    },
    actions: {
        // title: async ({commit, state}, title) => {
        //     commit('title', title)
        // }
    }
}
