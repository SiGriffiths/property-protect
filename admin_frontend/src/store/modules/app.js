export default {
    namespaced: true,
    state: () => ({
        title: ""
    }),
    getters: {
        title: (state) => {
            return 'PP::Admin - '+state.title
        }
    },
    mutations: {
        title: (state, payload) => {
            state.title = payload
        },
    },
    actions: {
        title: async ({commit, state}, title) => {
            commit('title', title)
        }
    }
}
