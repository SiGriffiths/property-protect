import AccountAPI from '../../api/account/account'

export default {
    namespaced: true,

    state: {
        token: localStorage.getItem("PeopleProtectApp-token") || '',
        refreshToken: localStorage.getItem('PeopleProtectApp-refresh-token') || "",
        loggedInUser: {},
        isAuthenticated: false
    },

    getters: {
        isLoggedIn: state => !!state.token
    },

    mutations: {
        setTokens: (state, payload) => {
            state.token = payload.token
            state.refreshToken = payload.refreshToken
            localStorage.setItem('PeopleProtectApp-token', state.token)
            localStorage.setItem('PeopleProtectApp-refresh-token', state.refreshToken)
        },
        logOut: (state) => {
            state.token = ""
            state.refreshToken = ""
            state.loggedInUser = {}
            state.isAuthenticated = false
            localStorage.setItem('PeopleProtectApp-token', '')
            localStorage.setItem('PeopleProtectApp-refresh-token', '')
        }
    },

    actions: {
        login: async({commit, dispatch}, payload) => {
            return AccountAPI.login(payload.email, payload.password).then((res) => {
                commit('setTokens', {
                    token: res.data.token,
                    refreshToken: res.data.refresh_token
                })
            })
        },
        logout: ({commit}) => {
            commit('logOut')
        }
    }
}
