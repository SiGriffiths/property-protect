import { createStore } from 'vuex'

import App from './modules/app'
import AuthenticationModule from './modules/auth'
import OrganisationsModule from './modules/organisations'

export default new createStore({
    modules: {
        app: App,
        auth: AuthenticationModule,
        organisations: OrganisationsModule
    }
})
