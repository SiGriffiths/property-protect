import axios from 'axios'

export default  {
    login(email, password) {
        return axios.post('/login_check', {username: email, password:password}, {baseURL: process.env.VUE_APP_API_ENDPOINT})
    }
}
