#!/bin/bash

touch /opt/supervisor/run/supervisor.sock

while [ ! -f /home/api/vendor/autoload.php ]
do
  echo Waiting for autoload...
  sleep 5;
done;

supervisord -c /etc/supervisor/supervisord.conf

