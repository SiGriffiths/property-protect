<?php

$config = new PhpCsFixer\Config();
return $config
    ->setRules([
        '@Symfony' => true,
        '@PSR12' => true,
        'no_superfluous_phpdoc_tags' => false,
        'single_line_throw' => false,
        'phpdoc_trim_consecutive_blank_line_separation' => false,
        'array_syntax' => ['syntax' => 'short'],
        'ordered_class_elements' => ['sort_algorithm' => 'alpha'],
        'ordered_imports' => ['sort_algorithm' => 'length'],
        'declare_strict_types' => true,
    ])->setRiskyAllowed(true);


