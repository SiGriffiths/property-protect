<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Infrastructure\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use CrookedSpire\Account\Domain\Value\AccountState as AccountStateValueObject;

final class AccountState extends Type
{
    private const TYPE = 'account_state';

    public function convertToPHPValue($value, AbstractPlatform $platform): AccountStateValueObject
    {
        return AccountStateValueObject::fromString($value);
    }

    public function getName(): string
    {
        return self::TYPE;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(100)';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
