<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Infrastructure\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use CrookedSpire\Account\Domain\Value\Name as NameValueObject;

final class Name extends Type
{
    private const TYPE = 'account_name';

    public function convertToPHPValue($value, AbstractPlatform $platform): NameValueObject
    {
        return NameValueObject::fromString($value);
    }

    public function getName(): string
    {
        return self::TYPE;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(255)';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
