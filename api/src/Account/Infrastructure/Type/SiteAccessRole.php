<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Infrastructure\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use CrookedSpire\Account\Domain\Value\SiteAccessRole as SiteAccessRoleValueObject;

final class SiteAccessRole extends Type
{
    private const TYPE = 'account_site_access_role';

    public function convertToPHPValue($value, AbstractPlatform $platform): ?SiteAccessRoleValueObject
    {
        return SiteAccessRoleValueObject::fromString($value);
    }

    public function getName(): string
    {
        return self::TYPE;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(100)';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
