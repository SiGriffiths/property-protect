<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Infrastructure\Repository;

use Symfony\Component\Uid\Uuid;
use Doctrine\Persistence\ManagerRegistry;
use CrookedSpire\Account\Domain\Entity\Verification;
use CrookedSpire\Account\Domain\Value\Verification\VerificationState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use CrookedSpire\Account\Domain\Repository\VerificationRepositoryInterface;

class VerificationRepository extends ServiceEntityRepository implements VerificationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Verification::class);
    }

    public function findById(Uuid $id): ?Verification
    {
        return $this->find($id);
    }

    public function findByToken(Uuid $token): ?Verification
    {
        return $this->findOneBy(['token' => $token]);
    }

    public function findUnclaimedByAccountId(Uuid $accountId): ?Verification
    {
        return $this->findOneBy([
            'state' => VerificationState::UNCLAIMED,
            'accountId' => $accountId,
        ]);
    }

    public function save(Verification $verification): void
    {
        $this->_em->persist($verification);
        $this->_em->flush();
    }
}
