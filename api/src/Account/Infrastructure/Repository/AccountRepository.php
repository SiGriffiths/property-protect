<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Infrastructure\Repository;

use Symfony\Component\Uid\Uuid;
use Doctrine\Persistence\ManagerRegistry;
use CrookedSpire\Account\Domain\Entity\Account;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class AccountRepository extends ServiceEntityRepository implements AccountRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Account::class);
    }

    public function findByEmail(string $email): ?Account
    {
        return $this->findOneBy(['email' => $email]);
    }

    public function findById(Uuid $id): ?Account
    {
        return $this->find($id);
    }

    public function save(Account $account): void
    {
        $this->_em->persist($account);
        $this->_em->flush();
    }
}
