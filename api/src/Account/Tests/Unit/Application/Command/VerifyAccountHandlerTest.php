<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Tests\Unit\Application\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Entity\Owner;
use Symfony\Component\Workflow\WorkflowInterface;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Entity\Verification;
use CrookedSpire\Account\Application\Command\VerifyAccount;
use CrookedSpire\Account\Application\Command\VerifyAccountHandler;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;
use CrookedSpire\Account\Infrastructure\Repository\VerificationRepository;

final class VerifyAccountHandlerTest extends TestCase
{
    public function testCanHandle(): void
    {
        $id = Uuid::v4();

        $account = Owner::create(
            $id,
            Email::fromString('test@test.com'),
            Name::fromString('Testing Test'),
            AccountState::fromString(AccountState::UNVERIFIED),
            'testing-testing-testing'
        );

        $accountRepository = $this->createMock(AccountRepositoryInterface::class);
        $accountRepository->expects(self::once())
            ->method('findById')
            ->willReturn($account);

        $accountRepository->expects(self::once())
            ->method('save');

        $verification = Verification::create(
            Uuid::v4(),
            $account->id(),
            Uuid::v4()
        );
        $verificationRepository = $this->createMock(VerificationRepository::class);
        $verificationRepository->expects(self::once())
            ->method('findByToken')
            ->willReturn($verification);

        $accountStateMachine = $this->createMock(WorkflowInterface::class);
        $accountStateMachine->expects(self::once())
            ->method('can')
            ->willReturn(true);

        $verificationStateMachine = $this->createMock(WorkflowInterface::class);
        $verificationStateMachine->expects(self::once())
            ->method('can')
            ->willReturn(true);

        $handler = new VerifyAccountHandler(
            $accountRepository,
            $verificationRepository,
            $accountStateMachine,
            $verificationStateMachine
        );
        $command = new VerifyAccount($id);

        $handler->__invoke($command);
    }

    public function testInvalidStateFromUnverifiedToDeleted(): void
    {
        $id = Uuid::v4();

        $account = Owner::create(
            $id,
            Email::fromString('test@test.com'),
            Name::fromString('Testing Test'),
            AccountState::fromString(AccountState::UNVERIFIED),
            'testing-testing-testing'
        );

        $accountRepository = $this->createMock(AccountRepositoryInterface::class);
        $accountRepository->expects(self::once())
            ->method('findById')
            ->willReturn($account);

        $verification = Verification::create(
            Uuid::v4(),
            $account->id(),
            Uuid::v4()
        );
        $verificationRepository = $this->createMock(VerificationRepository::class);
        $verificationRepository->expects(self::once())
            ->method('findByToken')
            ->willReturn($verification);

        $accountStateMachine = $this->createMock(WorkflowInterface::class);

        $verificationStateMachine = $this->createMock(WorkflowInterface::class);
        $verificationStateMachine->expects(self::once())
            ->method('can')
            ->willReturn(false);

        $handler = new VerifyAccountHandler(
            $accountRepository,
            $verificationRepository,
            $accountStateMachine,
            $verificationStateMachine
        );

        $command = new VerifyAccount($id);

        $this->expectException(\Exception::class);
        $handler->__invoke($command);
    }
}
