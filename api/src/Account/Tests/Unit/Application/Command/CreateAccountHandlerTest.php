<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Tests\Unit\Application\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Entity\Owner;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Specification\EmailIsUnique;
use CrookedSpire\Account\Application\Command\CreateOwnerAccount;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;
use CrookedSpire\Account\Application\Command\CreateOwnerAccountHandler;

final class CreateAccountHandlerTest extends TestCase
{
    public function testCanHandle(): void
    {
        $id = Uuid::v4();

        $accountRepository = $this->createMock(AccountRepositoryInterface::class);
        $accountRepository->expects(self::once())
            ->method('findByEmail')
            ->willReturn(null);

        $accountRepository->expects(self::once())
            ->method('save');

        $emailIsUniqueSpec = new EmailIsUnique($accountRepository);
        $handler = new CreateOwnerAccountHandler($accountRepository, $emailIsUniqueSpec);
        $command = new CreateOwnerAccount(
            $id,
            'test@test.com',
            'Testing Test',
            'testing-testing-testing'
        );

        $handler->__invoke($command);
    }

    public function testCannotHandleDuplicateEmail(): void
    {
        $id = Uuid::v4();

        $account = Owner::create(
            $id,
            Email::fromString('test@test.com'),
            Name::fromString('Testing Test'),
            AccountState::fromString(AccountState::UNVERIFIED),
            'testing-testing-testing'
        );

        $accountRepository = $this->createMock(AccountRepositoryInterface::class);
        $accountRepository->expects(self::once())
            ->method('findByEmail')
            ->willReturn($account);

        $emailIsUniqueSpec = new EmailIsUnique($accountRepository);

        $handler = new CreateOwnerAccountHandler($accountRepository, $emailIsUniqueSpec);
        $command = new CreateOwnerAccount(
            $id,
            'test@test.com',
            'Testing Test',
            'testing-testing-testing'
        );

        $this->expectException(\RuntimeException::class);
        $handler->__invoke($command);
    }
}
