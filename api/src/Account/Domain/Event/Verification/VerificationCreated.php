<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Event\Verification;

use CrookedSpire\PropertyProtect\Common\Domain\Event\AbstractDomainEvent;

final class VerificationCreated extends AbstractDomainEvent
{
    public function __construct(
        protected string $aggregateId,
        private readonly string $accountId,
        private readonly string $token
    ) {
        parent::__construct();
    }

    public function accountId(): string
    {
        return $this->accountId;
    }

    public function body(): array
    {
        return [
            'accountId' => $this->accountId(),
            'token' => $this->token(),
        ];
    }

    public function token(): string
    {
        return $this->token;
    }
}
