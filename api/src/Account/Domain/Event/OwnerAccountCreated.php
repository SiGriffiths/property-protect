<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Event;

use CrookedSpire\PropertyProtect\Common\Domain\Event\AbstractDomainEvent;

final class OwnerAccountCreated extends AbstractDomainEvent implements AccountInformationEventInterface
{
    public function __construct(
        protected string $aggregateId,
        private readonly string $email,
        private readonly string $name
    ) {
        parent::__construct();
    }

    public function body(): array
    {
        return [
            'email' => $this->email(),
            'name' => $this->name(),
        ];
    }

    public function email(): string
    {
        return $this->email;
    }

    public function name(): string
    {
        return $this->name;
    }
}
