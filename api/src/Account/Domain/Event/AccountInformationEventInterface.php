<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Event;

interface AccountInformationEventInterface
{
    public function aggregateId(): string;

    public function email(): string;

    public function name(): string;
}
