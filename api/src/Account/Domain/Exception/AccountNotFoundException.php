<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Exception;

final class AccountNotFoundException extends \RuntimeException
{
    public static function accountNotFound(string $accountId): AccountNotFoundException
    {
        return new self(
            sprintf(
                'Account not found for uuid: "%s"',
                $accountId
            )
        );
    }
}
