<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Exception;

final class InvalidSiteAccessRoleException extends \RuntimeException
{
    public static function invalidSiteAccessRole(string $role): InvalidSiteAccessRoleException
    {
        return new self(
            sprintf(
                'Invalid site access role requested: "%s"',
                $role
            )
        );
    }
}
