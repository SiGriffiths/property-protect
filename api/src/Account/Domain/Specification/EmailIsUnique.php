<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Specification;

use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;

final class EmailIsUnique
{
    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository
    ) {
    }

    public function isSatisfiedBy(string $email): bool
    {
        $exists = $this->accountRepository->findByEmail($email);

        return null === $exists;
    }
}
