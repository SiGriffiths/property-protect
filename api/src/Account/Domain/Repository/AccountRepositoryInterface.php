<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Repository;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Entity\Account;

interface AccountRepositoryInterface
{
    public function findByEmail(string $email): ?Account;

    public function findById(Uuid $id): ?Account;

    public function save(Account $account): void;
}
