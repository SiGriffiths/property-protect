<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Repository;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Entity\Verification;

interface VerificationRepositoryInterface
{
    public function findById(Uuid $id): ?Verification;

    public function findByToken(Uuid $token): ?Verification;

    public function findUnclaimedByAccountId(Uuid $accountId): ?Verification;

    public function save(Verification $verification): void;
}
