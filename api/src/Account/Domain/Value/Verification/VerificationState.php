<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Value\Verification;

use CrookedSpire\Account\Domain\Exception\InvalidStateException;

final class VerificationState
{
    public const CLAIMED = 'claimed';

    public const INVALIDATED = 'invalidated';
    public const TRANSITION_PREFIX = 'to_';
    public const UNCLAIMED = 'unclaimed';

    private static array $map = [
        self::UNCLAIMED,
        self::CLAIMED,
        self::INVALIDATED,
    ];

    private function __construct(
        private readonly string $state
    ) {
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public static function fromString(string $state): self
    {
        if (!in_array($state, self::$map, true)) {
            throw InvalidStateException::invalidStateRequested($state);
        }

        return new self($state);
    }

    public function value(): string
    {
        return $this->state;
    }
}
