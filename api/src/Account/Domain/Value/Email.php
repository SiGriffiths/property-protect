<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Value;

final class Email
{
    private function __construct(
        private readonly string $email
    ) {
        if (false === filter_var($email, FILTER_VALIDATE_EMAIL) || '.' === $email[strlen($email) - 2]) {
            throw new \InvalidArgumentException('Email address is invalid');
        }
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public function equals(Email $email): bool
    {
        return $this->value() === $email->value();
    }

    public static function fromString(string $email): self
    {
        return new self($email);
    }

    public function value(): string
    {
        return $this->email;
    }
}
