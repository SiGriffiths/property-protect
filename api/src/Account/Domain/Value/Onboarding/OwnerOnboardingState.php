<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Value\Onboarding;

use CrookedSpire\Account\Domain\Exception\InvalidStateException;

final class OwnerOnboardingState
{
    public const COMPANY = 'company';
    public const NEW = 'new';

    public const PAYMENT = 'payment';
    public const TRANSITION_PREFIX = 'to_';
    public const VALID = 'valid';

    private static array $map = [
        self::NEW,
        self::COMPANY,
        self::PAYMENT,
        self::VALID,
    ];

    private function __construct(
        private readonly string $state
    ) {
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public static function fromString(string $state): self
    {
        if (!in_array($state, self::$map, true)) {
            throw InvalidStateException::invalidStateRequested($state);
        }

        return new self($state);
    }

    public function value(): string
    {
        return $this->state;
    }
}
