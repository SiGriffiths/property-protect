<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Value;

final class Name
{
    private function __construct(
        private readonly string $name
    ) {
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public function equals(Name $name): bool
    {
        return $this->value() === $name->value();
    }

    public static function fromString(string $name): self
    {
        return new self($name);
    }

    public function value(): string
    {
        return $this->name;
    }
}
