<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Value;

use CrookedSpire\Account\Domain\Exception\InvalidStateException;

final class AccountState
{
    public const CLOSED = 'closed';
    public const DELETED = 'deleted';
    public const TRANSITION_PREFIX = 'to_';

    public const UNVERIFIED = 'unverified';
    public const VERIFIED = 'verified';

    private static array $map = [
        self::UNVERIFIED,
        self::VERIFIED,
        self::CLOSED,
        self::DELETED,
    ];

    private function __construct(
        private readonly string $state
    ) {
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public static function fromString(string $state): self
    {
        if (!in_array($state, self::$map, true)) {
            throw InvalidStateException::invalidStateRequested($state);
        }

        return new self($state);
    }

    public function value(): string
    {
        return $this->state;
    }
}
