<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Value;

use CrookedSpire\Account\Domain\Exception\InvalidSiteAccessRoleException;

final class SiteAccessRole
{
    public const ALLOWED_SITE_ROLES = [
        self::ROLE_NO_ACCESS,
        self::ROLE_ACCESS_ADMIN,
        self::ROLE_ACCESS_CUSTOMER_SERVICE,
        self::ROLE_ACCESS_CUSTOMER,
        self::ROLE_ACCESS_TENANT,
    ];

    public const ROLE_ACCESS_ADMIN = 'ROLE_ACCESS_ADMIN';
    public const ROLE_ACCESS_CUSTOMER = 'ROLE_ACCESS_CUSTOMER';
    public const ROLE_ACCESS_CUSTOMER_SERVICE = 'ROLE_ACCESS_CUSTOMER_SERVICE';
    public const ROLE_ACCESS_TENANT = 'ROLE_ACCESS_TENANT';
    public const ROLE_NO_ACCESS = 'ROLE_NO_ACCESS';

    private function __construct(
        private readonly string $role
    ) {
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public function equals(SiteAccessRole $role): bool
    {
        return $this->value() === $role->value();
    }

    public static function fromString(string $role): self
    {
        if (!in_array($role, self::ALLOWED_SITE_ROLES)) {
            throw InvalidSiteAccessRoleException::invalidSiteAccessRole($role);
        }

        return new self($role);
    }

    public function value(): string
    {
        return $this->role;
    }
}
