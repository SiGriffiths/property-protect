<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Value\AccountState;

class Internal extends Account
{
    public static function create(
        Uuid $id,
        Email $email,
        Name $name,
        AccountState $state,
        string $hashedPassword
    ): self {
        return new self($id, $email, $name, $state, $hashedPassword);
    }
}
