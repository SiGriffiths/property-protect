<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Value\SiteAccessRole;
use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Account implements EventEmitterAwareInterface
{
    use EventAwareEmitter;
    protected string $accountType;

    protected \DateTimeImmutable $createdAt;
    protected SiteAccessRole $siteAccessRole;

    protected function __construct(
        protected Uuid $id,
        protected Email $email,
        protected Name $name,
        protected AccountState $state,
        protected string $hashedPassword,
    ) {
        $this->createdAt = new \DateTimeImmutable();
        $this->siteAccessRole = SiteAccessRole::fromString(SiteAccessRole::ROLE_NO_ACCESS);
    }

    public function accountType(): string
    {
        return $this->accountType;
    }

    public function changeSiteAccessRole(SiteAccessRole $siteAccessRole): Account
    {
        $this->siteAccessRole = $siteAccessRole;

        // @todo emit role added domain event

        return $this;
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function hashedPassword(): string
    {
        return $this->hashedPassword;
    }

    public function id(): Uuid
    {
        return $this->id;
    }

    public function name(): Name
    {
        return $this->name;
    }

    public function siteAccessRole(): SiteAccessRole
    {
        return $this->siteAccessRole;
    }

    public function state(): AccountState
    {
        return $this->state;
    }

    public function updateHashedPassword(string $hashedPassword): self
    {
        $this->hashedPassword = $hashedPassword;

        return $this;
    }

    public function updateState(AccountState $state): self
    {
        $this->state = $state;
        // @todo emit state change event
        return $this;
    }
}
