<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Value\Verification\VerificationState;

class Verification
{
    private ?\DateTimeImmutable $claimedOn;
    private \DateTimeImmutable $createdAt;
    private VerificationState $state;

    private function __construct(
        private readonly Uuid $id,
        private readonly Uuid $accountId,
        private readonly Uuid $token
    ) {
        $this->createdAt = new \DateTimeImmutable();
        $this->state = VerificationState::fromString(VerificationState::UNCLAIMED);
    }

    public function accountId(): Uuid
    {
        return $this->accountId;
    }

    public function claimedOn(): ?\DateTimeImmutable
    {
        return $this->claimedOn;
    }

    public static function create(
        Uuid $id,
        Uuid $accountId,
        Uuid $token
    ): self {
        $verification = new self($id, $accountId, $token);

        // emit domain event

        return $verification;
    }

    public function createdAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function id(): Uuid
    {
        return $this->id;
    }

    public function state(): VerificationState
    {
        return $this->state;
    }

    public function token(): Uuid
    {
        return $this->token;
    }

    public function updateState(VerificationState $state): self
    {
        if (VerificationState::CLAIMED === $state->value()) {
            $this->claimedOn = new \DateTimeImmutable();
        }

        $this->state = $state;

        return $this;
    }
}
