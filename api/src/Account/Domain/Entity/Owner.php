<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Event\OwnerAccountCreated;
use CrookedSpire\Account\Domain\Value\Onboarding\OwnerOnboardingState;

class Owner extends Account
{
    private string $onboarding;
    private ?Uuid $organisationId;

    public function changeOrganisation(Uuid $id): self
    {
        $this->organisationId = $id;

        return $this;
    }

    public static function create(
        Uuid $id,
        Uuid $organisationId,
        Email $email,
        Name $name,
        AccountState $state,
        string $hashedPassword
    ): self {
        $owner = new self($id, $email, $name, $state, $hashedPassword);
        $owner->organisationId = $organisationId;

        $owner->onboarding = OwnerOnboardingState::NEW;

        $owner->attachEvent(new OwnerAccountCreated(
            $id->toRfc4122(),
            $email->value(),
            $name->value()
        ));

        return $owner;
    }

    public function onboarding(): string
    {
        return $this->onboarding;
    }

    public function organisation(): ?Uuid
    {
        return $this->organisationId;
    }

    public function updateOnboarding(string $state): self
    {
        $this->onboarding = $state;

        return $this;
    }
}
