<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Workflow\Onboarding;

use CrookedSpire\Account\Domain\Value\Onboarding\OwnerOnboardingState;
use CrookedSpire\PropertyProtect\Common\Application\Workflow\AbstractStateMarkingStore;

final class OwnerMarkingStore extends AbstractStateMarkingStore
{
    public const MUTATE_METHOD = 'changeOnboarding';
    public const READ_METHOD = 'onboarding';
    public const STATIC_CLASS = OwnerOnboardingState::class;
    public const STATIC_METHOD = 'fromString';
}
