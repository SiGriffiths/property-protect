<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Workflow;

use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\PropertyProtect\Common\Application\Workflow\AbstractStateMarkingStore;

final class AccountStateMarkingStore extends AbstractStateMarkingStore
{
    public const MUTATE_METHOD = 'updateState';
    public const READ_METHOD = 'state';
    public const STATIC_CLASS = AccountState::class;
    public const STATIC_METHOD = 'fromString';
}
