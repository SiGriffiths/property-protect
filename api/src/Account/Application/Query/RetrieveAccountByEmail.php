<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Query;

class RetrieveAccountByEmail
{
    public function __construct(
        public readonly string $email
    ) {
    }
}
