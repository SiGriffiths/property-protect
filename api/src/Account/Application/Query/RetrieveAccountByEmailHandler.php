<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Query;

use CrookedSpire\Account\Domain\Entity\Account;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;

class RetrieveAccountByEmailHandler
{
    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository
    ) {
    }

    public function __invoke(RetrieveAccountByEmail $query): ?Account
    {
        return $this->accountRepository->findByEmail($query->email);
    }
}
