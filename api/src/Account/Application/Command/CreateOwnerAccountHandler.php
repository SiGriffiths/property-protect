<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Entity\Owner;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Specification\EmailIsUnique;
use CrookedSpire\Account\Application\Service\PasswordHashService;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;

class CreateOwnerAccountHandler
{
    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository,
        private readonly EmailIsUnique $emailIsUniqueSpec
    ) {
    }

    public function __invoke(CreateOwnerAccount $command): void
    {
        if (!$this->emailIsUniqueSpec->isSatisfiedBy($command->email)) {
            throw new \RuntimeException('Account with email already exists');
        }

        $account = Owner::create(
            $command->id,
            $command->organisationId,
            Email::fromString($command->email),
            Name::fromString($command->name),
            AccountState::fromString(AccountState::UNVERIFIED),
            PasswordHashService::hashPassword($command->password)
        );

        $this->accountRepository->save($account);
    }
}
