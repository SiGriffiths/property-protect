<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use CrookedSpire\Account\Domain\Value\Name;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Entity\Internal;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Value\SiteAccessRole;
use CrookedSpire\Account\Domain\Specification\EmailIsUnique;
use CrookedSpire\Account\Application\Service\PasswordHashService;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;

class CreateInternalAccountHandler
{
    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository,
        private readonly EmailIsUnique $emailIsUniqueSpec
    ) {
    }

    public function __invoke(CreateInternalAccount $command): void
    {
        if (!$this->emailIsUniqueSpec->isSatisfiedBy($command->email)) {
            throw new \RuntimeException('Account with email already exists');
        }

        $account = Internal::create(
            $command->id,
            Email::fromString($command->email),
            Name::fromString($command->name),
            AccountState::fromString(AccountState::VERIFIED),
            PasswordHashService::hashPassword($command->password)
        );

        if ($command->siteAccessRole) {
            $account->changeSiteAccessRole(SiteAccessRole::fromString($command->siteAccessRole));
        }

        $this->accountRepository->save($account);
    }
}
