<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use Symfony\Component\Uid\Uuid;

class CreateInternalAccount
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $email,
        public readonly string $name,
        public readonly string $password,
        public readonly ?string $siteAccessRole
    ) {
    }
}
