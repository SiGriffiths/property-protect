<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use Symfony\Component\Uid\Uuid;

final class ChangeSiteAccessRole
{
    public function __construct(
        public readonly Uuid $accountId,
        public readonly string $siteAccessRole
    ) {
    }
}
