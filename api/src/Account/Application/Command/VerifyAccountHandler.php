<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use Symfony\Component\Workflow\WorkflowInterface;
use CrookedSpire\Account\Domain\Value\AccountState;
use CrookedSpire\Account\Domain\Value\Verification\VerificationState;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;
use CrookedSpire\Account\Domain\Repository\VerificationRepositoryInterface;

class VerifyAccountHandler
{
    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository,
        private readonly VerificationRepositoryInterface $verificationRepository,
        private readonly WorkflowInterface $accountStateMachine,
        private readonly WorkflowInterface $accountVerificationStateMachine
    ) {
    }

    public function __invoke(VerifyAccount $command): void
    {
        $verification = $this->verificationRepository->findByToken($command->tokenId);
        if (null === $verification) {
            // @todo create domain execption
            throw new \Exception('Not found');
        }

        $account = $this->accountRepository->findById($verification->accountId());
        if (null === $account) {
            throw new \Exception('Account not found');
        }

        if (false === $this->accountVerificationStateMachine->can(
            $verification,
            VerificationState::TRANSITION_PREFIX.VerificationState::CLAIMED
        )) {
            throw new \Exception('State verification');
        }

        if (false === $this->accountStateMachine->can(
            $account,
            AccountState::TRANSITION_PREFIX.AccountState::VERIFIED
        )) {
            throw new \Exception('State account');
        }

        $this->accountVerificationStateMachine->apply($verification, VerificationState::TRANSITION_PREFIX.VerificationState::CLAIMED);
        $this->accountStateMachine->apply($account, AccountState::TRANSITION_PREFIX.AccountState::VERIFIED);

        $this->verificationRepository->save($verification);
        $this->accountRepository->save($account);
    }
}
