<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use Symfony\Component\Uid\Uuid;

class CreateOwnerAccount
{
    public function __construct(
        public readonly Uuid $id,
        public readonly Uuid $organisationId,
        public readonly string $email,
        public readonly string $name,
        public readonly string $password
    ) {
    }
}
