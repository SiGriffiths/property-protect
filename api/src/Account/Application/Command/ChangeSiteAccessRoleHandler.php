<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use CrookedSpire\Account\Domain\Value\SiteAccessRole;
use CrookedSpire\Account\Domain\Exception\AccountNotFoundException;
use CrookedSpire\Account\Domain\Repository\AccountRepositoryInterface;

final class ChangeSiteAccessRoleHandler
{
    public function __construct(
        private readonly AccountRepositoryInterface $accountRepository,
    ) {
    }

    public function __invoke(ChangeSiteAccessRole $command): void
    {
        $account = $this->accountRepository->findById($command->accountId);

        if (null === $account) {
            throw AccountNotFoundException::accountNotFound($command->accountId->toRfc4122());
        }

        $account->changeSiteAccessRole(
            SiteAccessRole::fromString($command->siteAccessRole)
        );

        $this->accountRepository->save($account);
    }
}
