<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Command;

use Symfony\Component\Uid\Uuid;

class VerifyAccount
{
    public function __construct(
        public readonly Uuid $tokenId
    ) {
    }
}
