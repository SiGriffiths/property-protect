<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Notification;

use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\MailerInterface;
use CrookedSpire\PropertyProtect\Common\Application\Router\FrontendRoutes;

final class SendVerificationEmailHandler
{
    public function __construct(
        private readonly MailerInterface $mailer
    ) {
    }

    public function __invoke(SendVerificationEmail $notification): void
    {
        $url = FrontendRoutes::getRoute(FrontendRoutes::ACCOUNT_VERIFICATION, [$notification->token]);

        $email = (new Email())
            ->from('hello@propertyprotect.dev')
            ->to($notification->email)
            ->replyTo('support@propertyprotect.dev')
            ->subject('Please verify your account')
            ->text('Please verify your account.  Goto '.$url)
            ->html('<p>Please verify your account</p><a href="'.$url.'">Verify</a>');

        $this->mailer->send($email);
    }
}
