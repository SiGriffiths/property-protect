<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Notification;

use CrookedSpire\PropertyProtect\Common\Domain\Event\NotificationInterface;

final class SendVerificationEmail implements NotificationInterface
{
    public function __construct(
        public string $email,
        public string $name,
        public string $token
    ) {
    }
}
