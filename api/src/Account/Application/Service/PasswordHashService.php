<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Service;

class PasswordHashService
{
    public static function hashPassword(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public static function verifyPassword(string $hashedPassword, string $plainPassword): bool
    {
        return password_verify($hashedPassword, $plainPassword);
    }
}
