<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Service;

use CrookedSpire\Account\Domain\Value\AccountState;
use Symfony\Component\Security\Core\User\UserInterface;
use CrookedSpire\Account\Application\Dto\AccountCredentials;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use CrookedSpire\Account\Application\Query\RetrieveAccountByEmail;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\QueryBusInterface;

class AuthenticationProviderService implements UserProviderInterface
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $account = $this->queryBus->query(new RetrieveAccountByEmail($identifier));

        if (null === $account) {
            throw new \InvalidArgumentException('Account must exist');
        }

        if (AccountState::VERIFIED !== $account->state()->value()) {
            throw new \RuntimeException('Account must be verified');
        }

        return AccountCredentials::createFromAccount($account);
    }

    public function refreshUser(UserInterface $user): UserInterface
    {
        // JWT is stateless so this is never called
        return $user;
    }

    public function supportsClass(string $class): bool
    {
        return is_a($class, AccountCredentials::class, true);
    }
}
