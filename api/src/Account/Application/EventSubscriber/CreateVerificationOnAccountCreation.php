<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\EventSubscriber;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Account\Domain\Entity\Verification;
use Symfony\Component\Messenger\MessageBusInterface;
use CrookedSpire\Account\Domain\Event\OwnerAccountCreated;
use CrookedSpire\Account\Domain\Event\TenantAccountCreated;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;
use CrookedSpire\Account\Domain\Event\AccountInformationEventInterface;
use CrookedSpire\Account\Application\Notification\SendVerificationEmail;
use CrookedSpire\Account\Domain\Repository\VerificationRepositoryInterface;

final class CreateVerificationOnAccountCreation implements MessageSubscriberInterface
{
    public function __construct(
        private readonly VerificationRepositoryInterface $verificationRepository,
        private readonly MessageBusInterface $notificationBus
    ) {
    }

    public function createVerification(AccountInformationEventInterface $event): void
    {
        $verification = Verification::create(
            Uuid::v4(),
            Uuid::fromString($event->aggregateId()),
            Uuid::v4()
        );

        $this->verificationRepository->save($verification);

        $this->notificationBus->dispatch(new SendVerificationEmail(
            $event->email(),
            $event->name(),
            $verification->token()->toRfc4122()
        ));
    }

    public static function getHandledMessages(): iterable
    {
        yield OwnerAccountCreated::class => [
            'method' => 'createVerification',
        ];
        yield TenantAccountCreated::class => [
            'method' => 'createVerification',
        ];
    }
}
