<?php

declare(strict_types=1);

namespace CrookedSpire\Account\Application\Dto;

use CrookedSpire\Account\Domain\Entity\Account;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

class AccountCredentials implements UserInterface, PasswordAuthenticatedUserInterface
{
    private function __construct(
        private readonly Account $account,
        private readonly string $accountType
    ) {
    }

    public static function createFromAccount(Account $account): self
    {
        return new self($account, get_class($account));
    }

    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return string
     */
    public function getAccountType(): string
    {
        return $this->accountType;
    }

    public function getPassword(): ?string
    {
        return $this->account->hashedPassword();
    }

    public function getRoles(): array
    {
        return [$this->account->siteAccessRole()->value()];
    }

    public function getUserIdentifier(): string
    {
        return $this->account->email()->value();
    }

    public function getUsername(): string
    {
        return $this->getUserIdentifier();
    }
}
