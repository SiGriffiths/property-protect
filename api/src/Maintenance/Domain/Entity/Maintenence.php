<?php

declare(strict_types=1);

namespace CrookedSpire\Maintenance\Domain\Entity;

use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Maintenence implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct()
    {
        // name
        // description
        // type (gas check, smoke detectors, etc)
        // frequency (1 year, 10 year, six months etc)
        // property id
    }

    public static function create(): self
    {
        return new self();
    }
}
