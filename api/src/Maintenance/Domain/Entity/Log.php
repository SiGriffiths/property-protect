<?php

declare(strict_types=1);

namespace CrookedSpire\Maintenance\Domain\Entity;

use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Log implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct()
    {
        // maintenance id
        // property id
        // date due
        // status (completed, due, overdue)
    }

    public static function create(): self
    {
        return new self();
    }
}
