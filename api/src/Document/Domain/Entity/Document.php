<?php

declare(strict_types=1);

namespace CrookedSpire\Document\Domain\Entity;

use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Document implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct()
    {
    }

    public static function create(): self
    {
        return new self();
    }
}
