<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Application\Payload\Account;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class IsUuidValidator
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    public function validate(string $candidateToValidate): ConstraintViolationList
    {
        return $this->validator->validate($candidateToValidate, new Assert\Uuid());
    }
}
