<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Application\Payload\Account;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use CrookedSpire\PropertyProtect\Common\Application\Payload\ValidateInterface;

class CreateCustomerAccountValidator implements ValidateInterface
{
    public function __construct(
        private readonly ValidatorInterface $validator
    ) {
    }

    public function validate(array $payload): ConstraintViolationList
    {
        return $this->validator->validate($payload, $this->constraints());
    }

    private function constraints(): Assert\Collection
    {
        return new Assert\Collection([
            'email' => [
                new Assert\Type('string'),
                new Assert\Email(['mode' => 'html5']),
                new Assert\NotBlank(),
                new Assert\Required(),
                new Assert\Length(['min' => 6]),
            ],
            'name' => [
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Required(),
                new Assert\Length(['min' => 6]),
            ],
            'password' => [
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Required(),
                new Assert\Length(['min' => 8]),
            ],
            'companyName' => [
                new Assert\Type('string'),
                new Assert\NotBlank(),
                new Assert\Required(),
                new Assert\Length(['min' => 6]),
            ],
            'companyRegistrationNumber' => [
                new Assert\Optional([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 6]),
                ]),
            ],
            'companyVatNumber' => [
                new Assert\Optional([
                    new Assert\Type('string'),
                    new Assert\NotBlank(),
                    new Assert\Length(['min' => 4]),
                ]),
            ],
        ]);
    }
}
