<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Adapters\Http\Admin\Property;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\QueryBusInterface;

class ListController extends AbstractController
{
    public function __construct(
        private readonly QueryBusInterface $queryBus
    ) {
    }

    #[Route(path: '/api/admin/property/list', name: 'admin_property_list', methods: ['GET'])]
    public function __invoke(): JsonResponse
    {
        return new JsonResponse($this->queryBus->query(
//            new RetrieveOrganisations(),
        ), Response::HTTP_OK);
    }
}
