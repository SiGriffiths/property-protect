<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Adapters\Cli\Account;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use CrookedSpire\Account\Application\Command\CreateInternalAccount;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\CommandBusInterface;

class Create extends Command
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {
        parent::__construct('prpr:account:internal:create');
    }

    public function configure(): void
    {
        $this->addArgument('email', InputArgument::REQUIRED, 'An email must be provided')
            ->addArgument('name', InputArgument::REQUIRED, 'A name must be provided')
            ->addArgument('password', InputArgument::REQUIRED, 'A password must be provided')
            ->addArgument('siteAccessRole', InputArgument::OPTIONAL, 'Initial site access role');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = Uuid::v4();
        $this->commandBus->command(new CreateInternalAccount(
            $id,
            $input->getArgument('email'),
            $input->getArgument('name'),
            $input->getArgument('password'),
            $input->getArgument('siteAccessRole') ?? null
        ));

        $output->writeln($id->toRfc4122());

        return Command::SUCCESS;
    }
}
