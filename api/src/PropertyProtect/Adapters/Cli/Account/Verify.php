<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Adapters\Cli\Account;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use CrookedSpire\Account\Application\Command\VerifyAccount;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\CommandBusInterface;

class Verify extends Command
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {
        parent::__construct('prpr:account:verify');
    }

    public function configure(): void
    {
        $this->addArgument('uuid', InputArgument::REQUIRED, 'An customer uuid must be provided');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $uuid = $input->getArgument('uuid');
        $output->writeln('Processing: '.$uuid);
        $this->commandBus->command(new VerifyAccount(
            Uuid::fromString($uuid)
        ));

        return Command::SUCCESS;
    }
}
