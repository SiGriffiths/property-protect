<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Adapters\Cli\Account;

use Symfony\Component\Uid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use CrookedSpire\Account\Application\Command\ChangeSiteAccessRole;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\CommandBusInterface;

class ChangeRole extends Command
{
    public function __construct(
        private readonly CommandBusInterface $commandBus
    ) {
        parent::__construct('prpr:account:change-site-access-role');
    }

    public function configure(): void
    {
        $this->addArgument('uuid', InputArgument::REQUIRED, 'An customer uuid must be provided');
        $this->addArgument('role', InputArgument::REQUIRED, 'An role must be provided');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $uuid = $input->getArgument('uuid');
        $role = $input->getArgument('role');

        $output->writeln(sprintf('Processing %s for role %s', $uuid, $role));

        $this->commandBus->command(new ChangeSiteAccessRole(
            Uuid::fromString($uuid),
            $role
        ));

        return Command::SUCCESS;
    }
}
