<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Infrastructure\MessageBus;

use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\QueryBusInterface;

class QueryBus implements QueryBusInterface
{
    use HandleTrait;

    public function __construct(MessageBusInterface $queryBus)
    {
        $this->messageBus = $queryBus;
    }

    public function query($query)
    {
        return $this->handle($query);
    }
}
