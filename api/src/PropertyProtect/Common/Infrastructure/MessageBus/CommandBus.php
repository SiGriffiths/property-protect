<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Infrastructure\MessageBus;

use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use CrookedSpire\PropertyProtect\Common\Application\MessageBus\CommandBusInterface;

class CommandBus implements CommandBusInterface
{
    use HandleTrait;

    public function __construct(MessageBusInterface $commandBus)
    {
        $this->messageBus = $commandBus;
    }

    public function command($command)
    {
        return $this->handle($command);
    }
}
