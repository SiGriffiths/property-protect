<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Infrastructure\EventListener\Doctrine;

use Doctrine\ORM\Events;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;
use CrookedSpire\PropertyProtect\Common\Domain\Event\EventInterface;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

final class DoctrineEventPublisher implements EventSubscriber
{
    /** @var array<EventEmitterAwareInterface> */
    private array $entities = [];

    public function __construct(
        private MessageBusInterface $eventBus,
    ) {
    }

    public function getEntities(): array
    {
        return $this->entities;
    }

    public function getSubscribedEvents()
    {
        return [Events::prePersist, Events::preFlush, Events::postFlush];
    }

    public function postFlush(PostFlushEventArgs $event): void
    {
        $domainEvents = [];

        foreach ($this->getEntities() as $entity) {
            foreach ($entity->retrieveEvents() as $domainEvent) {
                $domainEvents[] = $domainEvent;
            }
        }
        usort($domainEvents, static function (EventInterface $a, EventInterface $b) {
            if ($a->createdAtAsFloat() === $b->createdAtAsFloat()) {
                return 0;
            }

            return $a->createdAtAsFloat() < $b->createdAtAsFloat() ? -1 : 1;
        });

        foreach ($domainEvents as $domainEvent) {
            $stamps = [new DispatchAfterCurrentBusStamp()];

            $this->eventBus->dispatch($domainEvent, $stamps);
        }

        $this->entities = [];
    }

    public function preFlush(PreFlushEventArgs $event): void
    {
        $uow = $event->getEntityManager()->getUnitOfWork();

        foreach ($uow->getIdentityMap() as $class => $entities) {
            if (!is_a($class, EventEmitterAwareInterface::class, true)) {
                continue;
            }
            /** @var EventEmitterAwareInterface $entity */
            foreach ($entities as $entity) {
                $this->entities[] = $entity;
            }
        }
    }

    public function prePersist(LifecycleEventArgs $event): void
    {
        $entity = $event->getEntity();

        if ($entity instanceof EventEmitterAwareInterface) {
            $this->entities[] = $entity;
        }
    }
}
