<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware;

use CrookedSpire\PropertyProtect\Common\Domain\Event\EventInterface;

trait EventAwareEmitter
{
    private array $events = [];

    public function attachEvent(EventInterface $event): void
    {
        $this->events[] = $event;
    }

    public function retrieveEvents(): array
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }
}
