<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Domain\Event;

interface EventInterface
{
    public function aggregateId(): string;

    public function body(): array;

    public function createdAtAsFloat(): float;

    public function eventName(): string;
}
