<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Domain\Event\Aware;

use CrookedSpire\PropertyProtect\Common\Domain\Event\EventInterface;

interface EventEmitterAwareInterface
{
    public function attachEvent(EventInterface $event): void;

    public function retrieveEvents(): array;
}
