<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Domain\Event;

class AbstractDomainEvent implements EventInterface, \JsonSerializable
{
    protected string $aggregateId;
    protected string $createdAt;
    protected string $eventName;
    protected string $source = 'CrookedSpire';
    protected string $type = 'domain_event';

    public function __construct()
    {
        $this->createdAt = (string) microtime(true);
        $this->eventName = (new \ReflectionClass($this))->getShortName();
    }

    public function aggregateId(): string
    {
        return $this->aggregateId;
    }

    public function body(): array
    {
        return [];
    }

    public function createdAtAsFloat(): float
    {
        return (float) $this->createdAt;
    }

    public function eventName(): string
    {
        return $this->eventName;
    }

    public function jsonSerialize(): array
    {
        return array_merge([
            'aggregateId' => $this->aggregateId,
            'name' => $this->eventName,
            'type' => $this->type,
            'source' => $this->source,
            'id' => $this->aggregateId,
            'createdAt' => $this->createdAt,
        ], $this->body());
    }
}
