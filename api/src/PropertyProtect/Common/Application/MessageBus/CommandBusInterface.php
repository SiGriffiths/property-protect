<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\MessageBus;

interface CommandBusInterface
{
    public function command($command);
}
