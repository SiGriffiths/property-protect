<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\MessageBus;

interface QueryBusInterface
{
    public function query($query);
}
