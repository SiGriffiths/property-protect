<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\Router;

final class FrontendRoutes
{
    public const ACCOUNT_VERIFICATION = '/auth/verify/';

    public static function getRoute(string $intendedRouteFragment, array $options = []): string
    {
        $url = $_ENV['FRONTEND_URL'].$intendedRouteFragment;

        foreach ($options as $key => $opt) {
            if ($key) {
                $url .= $key.'/'.$opt;
            } else {
                $url .= $opt;
            }
        }

        return $url;
    }
}
