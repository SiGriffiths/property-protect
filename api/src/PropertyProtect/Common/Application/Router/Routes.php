<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\Router;

final class Routes
{
    public const ACCOUNT_SIGNUP = '/account/signup';
    public const ACCOUNT_VERIFICATION = '/api/account/verify/{tokenId}';
}
