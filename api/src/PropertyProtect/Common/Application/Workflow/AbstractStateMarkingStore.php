<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\Workflow;

use Symfony\Component\Workflow\Marking;
use Symfony\Component\Workflow\Exception\LogicException;
use Symfony\Component\Workflow\MarkingStore\MarkingStoreInterface;

abstract class AbstractStateMarkingStore implements MarkingStoreInterface
{
    public const MUTATE_METHOD = '';
    public const READ_METHOD = '';
    public const STATIC_CLASS = '';
    public const STATIC_METHOD = '';

    public function __construct(
        private readonly bool $singleState = true
    ) {
    }

    public function getMarking(object $subject): Marking
    {
        $method = static::READ_METHOD;

        if (!method_exists($subject, $method)) {
            throw new LogicException(sprintf('The method "%s::%s()" does not exist.', get_debug_type($subject), $method));
        }

        $marking = null;
        try {
            $marking = $subject->{$method}()->value();
        } catch (\Error $e) {
            $unInitializedPropertyMassage = sprintf('Typed property %s::$%s must not be accessed before initialization', get_debug_type($subject), static::READ_METHOD);
            if ($e->getMessage() !== $unInitializedPropertyMassage) {
                throw $e;
            }
        }

        if (null === $marking) {
            return new Marking();
        }

        if ($this->singleState) {
            $marking = [(string) $marking => 1];
        }

        return new Marking($marking);
    }

    public function setMarking(object $subject, Marking $marking, array $context = [])
    {
        $marking = $marking->getPlaces();

        if ($this->singleState) {
            $marking = key($marking);
        }

        $method = static::MUTATE_METHOD;

        if (!method_exists($subject, $method)) {
            throw new LogicException(sprintf('The method "%s::%s()" does not exist.', get_debug_type($subject), $method));
        }
        $staticClass = static::STATIC_CLASS.'::'.static::STATIC_METHOD;

        $subject->{$method}($staticClass($marking), $context);
    }
}
