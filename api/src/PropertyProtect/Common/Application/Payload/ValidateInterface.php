<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\Payload;

use Symfony\Component\Validator\ConstraintViolationList;

interface ValidateInterface
{
    public function validate(array $payload): ConstraintViolationList;
}
