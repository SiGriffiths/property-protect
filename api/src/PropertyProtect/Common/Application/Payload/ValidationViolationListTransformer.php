<?php

declare(strict_types=1);

namespace CrookedSpire\PropertyProtect\Common\Application\Payload;

use Exception;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;

final class ValidationViolationListTransformer
{
    /**
     * @throws Exception
     */
    public static function convertToArray(ConstraintViolationList $list): array
    {
        return array_map(
            static fn (ConstraintViolation $violation): array => [
                'field' => str_replace('[', '', str_replace(']', '', $violation->getPropertyPath())),
                'message' => $violation->getMessage(),
            ],
            (array) $list->getIterator()
        );
    }
}
