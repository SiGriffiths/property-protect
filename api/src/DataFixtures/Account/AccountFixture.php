<?php

declare(strict_types=1);

namespace CrookedSpire\DataFixtures\Account;

use Symfony\Component\Uid\Uuid;
use Doctrine\Persistence\ObjectManager;
use CrookedSpire\Account\Domain\Value\Name;
use Doctrine\Bundle\FixturesBundle\Fixture;
use CrookedSpire\Account\Domain\Value\Email;
use CrookedSpire\Account\Domain\Entity\Owner;
use CrookedSpire\Account\Domain\Entity\Tenant;
use CrookedSpire\Account\Domain\Entity\Internal;
use CrookedSpire\Account\Domain\Value\AccountState;
use Symfony\Component\Messenger\MessageBusInterface;
use CrookedSpire\Account\Domain\Value\SiteAccessRole;
use CrookedSpire\Account\Application\Command\VerifyAccount;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use CrookedSpire\DataFixtures\Organisation\OrganisationFixture;
use CrookedSpire\Account\Application\Service\PasswordHashService;
use CrookedSpire\Account\Domain\Repository\VerificationRepositoryInterface;

class AccountFixture extends Fixture implements DependentFixtureInterface
{
    private const ADMIN_UUID = '6b3aceac-e108-485e-8281-cbab9e550836';
    private const CUSTOMER_SERVICE_UUID = '472170dc-fe0f-4bc9-9633-2ec276e7456b';

    private const DUMMY_PASSWORD = 'test1234';
    private const OWNER_UUID = '625576ae-be27-4e7a-8f30-ee9ed687ad60';
    private const TENANT_UUID = '1029cca9-330b-4bf9-8a26-66da0ce6f74b';

    public function __construct(
        private readonly VerificationRepositoryInterface $verificationRepository,
        private readonly MessageBusInterface $commandBus
    ) {
    }

    public function getDependencies(): array
    {
        return [
            OrganisationFixture::class,
        ];
    }

    public function load(ObjectManager $manager): void
    {
        $admin = Internal::create(
            Uuid::fromString(self::ADMIN_UUID),
            Email::fromString('admin@bingham.works'),
            Name::fromString('Super Admin'),
            AccountState::fromString(AccountState::VERIFIED),
            PasswordHashService::hashPassword(self::DUMMY_PASSWORD)
        );
        $admin->changeSiteAccessRole(SiteAccessRole::fromString(SiteAccessRole::ROLE_ACCESS_ADMIN));
        $manager->persist($admin);

        $customerService = Internal::create(
            Uuid::fromString(self::CUSTOMER_SERVICE_UUID),
            Email::fromString('support@bingham.works'),
            Name::fromString('Customer Service'),
            AccountState::fromString(AccountState::VERIFIED),
            PasswordHashService::hashPassword(self::DUMMY_PASSWORD)
        );
        $customerService->changeSiteAccessRole(SiteAccessRole::fromString(SiteAccessRole::ROLE_ACCESS_CUSTOMER_SERVICE));
        $manager->persist($customerService);

        $owner = Owner::create(
            Uuid::fromString(self::OWNER_UUID),
            Uuid::fromString(OrganisationFixture::ORGANISATION_UUID),
            Email::fromString('owner@bingham.works'),
            Name::fromString('Owner'),
            AccountState::fromString(AccountState::UNVERIFIED),
            PasswordHashService::hashPassword(self::DUMMY_PASSWORD)
        );
        $owner->changeSiteAccessRole(SiteAccessRole::fromString(SiteAccessRole::ROLE_ACCESS_CUSTOMER));
        $manager->persist($owner);

        $tenant = Tenant::create(
            Uuid::fromString(self::TENANT_UUID),
            Uuid::fromString(OrganisationFixture::ORGANISATION_UUID),
            Email::fromString('tenant@bingham.works'),
            Name::fromString('Tenant'),
            AccountState::fromString(AccountState::UNVERIFIED),
            PasswordHashService::hashPassword(self::DUMMY_PASSWORD)
        );
        $tenant->changeSiteAccessRole(SiteAccessRole::fromString(SiteAccessRole::ROLE_ACCESS_TENANT));
        $manager->persist($tenant);

        $manager->flush();

        // Account verifications
        // owner
        $verification = $this->verificationRepository->findUnclaimedByAccountId(Uuid::fromString(self::OWNER_UUID));
        $this->commandBus->dispatch(new VerifyAccount($verification->token()));

        // tenant
        $verification = $this->verificationRepository->findUnclaimedByAccountId(Uuid::fromString(self::TENANT_UUID));
        $this->commandBus->dispatch(new VerifyAccount($verification->token()));
    }
}
