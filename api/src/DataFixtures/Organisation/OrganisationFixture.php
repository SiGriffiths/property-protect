<?php

declare(strict_types=1);

namespace CrookedSpire\DataFixtures\Organisation;

use Symfony\Component\Uid\Uuid;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Messenger\MessageBusInterface;
use CrookedSpire\Organisation\Application\Command\CreateOrganisation;

class OrganisationFixture extends Fixture
{
    public const ORGANISATION_UUID = '6b3aceac-e108-485e-8281-cbab9e550836';

    public const REF_ORG_ID = 'organisation_id';
    private const NAME = 'Property Protect Test';

    public function __construct(
        private readonly MessageBusInterface $commandBus
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $id = $this->commandBus->dispatch(new CreateOrganisation(
            Uuid::fromString(self::ORGANISATION_UUID),
            self::NAME,
            '1234',
            '1234'
        ));

        $this->addReference(self::REF_ORG_ID, $id);
    }
}
