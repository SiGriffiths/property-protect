<?php

declare(strict_types=1);

namespace CrookedSpire\Permission\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

final class Resource implements EventEmitterAwareInterface
{
    use EventAwareEmitter;
    private int $active = 1;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct(
        private readonly Uuid $id,
        private readonly string $resource,
        private readonly string $area
    ) {
    }

    public function area(): string
    {
        return $this->area;
    }

    public function resource(): string
    {
        return $this->resource;
    }
}
