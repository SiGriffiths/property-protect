<?php

declare(strict_types=1);

namespace CrookedSpire\Permission\Domain\Entity;

use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

final class Limits implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct(
    ) {
    }
}
