<?php

declare(strict_types=1);

namespace CrookedSpire\Permission\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

final class Rule implements EventEmitterAwareInterface
{
    use EventAwareEmitter;
    private int $active = 1;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct(
        private readonly Uuid $id,
        private readonly Uuid $accountId,
        private readonly Uuid $organisationId,
        private readonly Uuid $teamId,
        private readonly Role $role
    ) {
    }
}
