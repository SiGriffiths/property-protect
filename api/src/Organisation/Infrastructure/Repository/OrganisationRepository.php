<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Infrastructure\Repository;

use Symfony\Component\Uid\Uuid;
use Doctrine\Persistence\ManagerRegistry;
use CrookedSpire\Organisation\Domain\Entity\Organisation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use CrookedSpire\Organisation\Domain\Repository\OrganisationRepositoryInterface;

class OrganisationRepository extends ServiceEntityRepository implements OrganisationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Organisation::class);
    }

    public function findById(Uuid $id): ?Organisation
    {
        return $this->find($id);
    }

    public function findByName(string $name): ?Organisation
    {
        return $this->findOneBy(['name' => $name]);
    }

    public function save(Organisation $organisation): void
    {
        $this->_em->persist($organisation);
        $this->_em->flush();
    }
}
