<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Infrastructure\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use CrookedSpire\Organisation\Domain\Value\OrganisationState as OrganisationStateValueObject;

final class OrganisationState extends Type
{
    private const TYPE = 'organisation_state';

    public function convertToPHPValue($value, AbstractPlatform $platform): OrganisationStateValueObject
    {
        return OrganisationStateValueObject::fromString($value);
    }

    public function getName(): string
    {
        return self::TYPE;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'VARCHAR(100)';
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform): bool
    {
        return true;
    }
}
