<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Application\Command;

use Symfony\Component\Uid\Uuid;

final class CreateOrganisation
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $name,
        public readonly ?string $companyNumber,
        public readonly ?string $vatNumber,
    ) {
    }
}
