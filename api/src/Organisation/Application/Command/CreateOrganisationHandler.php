<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Application\Command;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Organisation\Domain\Value\Name;
use CrookedSpire\Organisation\Domain\Entity\Organisation;
use CrookedSpire\Organisation\Domain\Value\OrganisationState;
use CrookedSpire\Organisation\Domain\Specification\OrganisationIsUnique;
use CrookedSpire\Organisation\Domain\Exception\OrganisationExistsException;
use CrookedSpire\Organisation\Domain\Repository\OrganisationRepositoryInterface;

final class CreateOrganisationHandler
{
    public function __construct(
        private readonly OrganisationRepositoryInterface $organisationRepository,
        private readonly OrganisationIsUnique $organisationIsUnique
    ) {
    }

    public function __invoke(CreateOrganisation $command): Uuid
    {
        if (!$this->organisationIsUnique->isSatisfiedBy($command->name)) {
            throw OrganisationExistsException::exists($command->name);
        }

        $organisation = Organisation::create(
            $command->id,
            Name::fromString($command->name),
            OrganisationState::fromString(OrganisationState::TRIAL),
            $command->companyNumber,
            $command->vatNumber
        );

        $this->organisationRepository->save($organisation);

        return $organisation->id();
    }
}
