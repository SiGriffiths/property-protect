<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Application\Query;

class RetrieveOrganisationByName
{
    public function __construct(
        public readonly string $name
    ) {
    }
}
