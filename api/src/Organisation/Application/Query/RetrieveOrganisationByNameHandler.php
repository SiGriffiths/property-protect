<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Application\Query;

use CrookedSpire\Organisation\Domain\Entity\Organisation;

class RetrieveOrganisationByNameHandler
{
    public function __construct(
    ) {
    }

    public function __invoke(RetrieveOrganisationByName $query): ?Organisation
    {
        return null;
    }
}
