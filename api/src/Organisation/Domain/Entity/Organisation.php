<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Entity;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Organisation\Domain\Value\Name;
use CrookedSpire\Organisation\Domain\Value\OrganisationState;
use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Organisation implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct(
        private readonly Uuid $id,
        private Name $name,
        private OrganisationState $state,
        private ?string $companyRegistrationNumber,
        private ?string $vatNumber
    ) {
        $this->createdAt = new \DateTimeImmutable();
        // address
    }

    public static function create(
        Uuid $id,
        Name $name,
        OrganisationState $state,
        ?string $companyRegistrationNumber,
        ?string $vatNumber
    ): self {
        $organisation = new self(
            $id,
            $name,
            $state,
            $companyRegistrationNumber,
            $vatNumber
        );

        // domain event

        return $organisation;
    }

    public function id(): Uuid
    {
        return $this->id;
    }
}
