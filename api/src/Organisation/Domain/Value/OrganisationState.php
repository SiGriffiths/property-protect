<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Value;

use CrookedSpire\Organisation\Domain\Exception\InvalidStateException;

final class OrganisationState
{
    public const ARREARS = 'arrears';
    public const DELETED = 'deleted';
    public const DISABLED = 'disabled';
    public const TRIAL = 'trial';
    public const VALID = 'valid';

    private static array $map = [
        self::VALID,
        self::ARREARS,
        self::TRIAL,
        self::DISABLED,
        self::DELETED,
    ];

    private function __construct(
        private readonly string $state
    ) {
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public static function fromString(string $state): self
    {
        if (!in_array($state, self::$map, true)) {
            throw InvalidStateException::invalidStateRequested($state);
        }

        return new self($state);
    }

    public function value(): string
    {
        return $this->state;
    }
}
