<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Value;

final class Name
{
    private function __construct(
        private string $name
    ) {
        $this->name = trim($name);
        if (strlen($this->name) < 3) {
            throw new \InvalidArgumentException('Name must be 4 characters or more in length');
        }
    }

    public function __toString(): string
    {
        return $this->value();
    }

    public function equals(Name $name): bool
    {
        return $this->value() === $name->value();
    }

    public static function fromString(string $name): self
    {
        return new self($name);
    }

    public function value(): string
    {
        return $this->name;
    }
}
