<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Specification;

use CrookedSpire\Organisation\Domain\Repository\OrganisationRepositoryInterface;

final class OrganisationIsUnique
{
    public function __construct(
        private readonly OrganisationRepositoryInterface $accountRepository
    ) {
    }

    public function isSatisfiedBy(string $name): bool
    {
        $exists = $this->accountRepository->findByName($name);

        return null === $exists;
    }
}
