<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Repository;

use Symfony\Component\Uid\Uuid;
use CrookedSpire\Organisation\Domain\Entity\Organisation;

interface OrganisationRepositoryInterface
{
    public function findById(Uuid $id): ?Organisation;

    public function findByName(string $name): ?Organisation;

    public function save(Organisation $organisation): void;
}
