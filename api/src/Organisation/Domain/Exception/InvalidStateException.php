<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Exception;

final class InvalidStateException extends \RuntimeException
{
    public static function invalidStateRequested(string $state): InvalidStateException
    {
        return new self(
            sprintf(
                'Invalid state requested: "%s"',
                $state
            )
        );
    }
}
