<?php

declare(strict_types=1);

namespace CrookedSpire\Organisation\Domain\Exception;

final class OrganisationExistsException extends \RuntimeException
{
    public static function exists(string $name): OrganisationExistsException
    {
        return new self(
            sprintf(
                'Organisation exists: "%s"',
                $name
            )
        );
    }
}
