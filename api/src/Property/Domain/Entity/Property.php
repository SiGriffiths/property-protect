<?php

declare(strict_types=1);

namespace CrookedSpire\Property\Domain\Entity;

use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Property implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct()
    {
        // type (detached, flat, semi, etc)
        // address
        // bedrooms
        // bathrooms
        // meta
        // council tax band
        // internet speed
        // school
        // floor plan
        // gallery
        // features
        // description
    }

    public static function create(): self
    {
        return new self();
    }
}
