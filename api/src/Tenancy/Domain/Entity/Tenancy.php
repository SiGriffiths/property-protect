<?php

declare(strict_types=1);

namespace CrookedSpire\Tenancy\Domain\Entity;

use CrookedSpire\PropertyProtect\Common\Infrastructure\Event\Aware\EventAwareEmitter;
use CrookedSpire\PropertyProtect\Common\Domain\Event\Aware\EventEmitterAwareInterface;

class Tenancy implements EventEmitterAwareInterface
{
    use EventAwareEmitter;

    private readonly \DateTimeImmutable $createdAt;

    private function __construct()
    {
        // start date
        // end date
        // tenant id
        // property id
        // type ( furnished / unfurnished )
        // documents
    }

    public static function create(): self
    {
        return new self();
    }
}
