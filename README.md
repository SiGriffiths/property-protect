# Property Protect
A DDD / Hexagonal / CQRS ish project to help other learn how to structure a symfony 6+ project in 2022.

Instead of another todo app or some banal example here we have a solid business pattern that 
has been done by many people, in a commercial setting.

This is an attempt at an open source UK property rental system.  Hopefully it will be more real than a simple
test app. SPA frontends fuelled by an api backend.  Eventually we'll get to projections and full async cqrs (maybe)

Feel free to ask questions / fork and play 

Lets GO YO!

Si

## Setup
`chown -R <your wsl/linux username>:<your wsl/linux username> api/var`

`docker-compose up`

`docker-compose exec propertyprotect_api /bin/bash`
`bin/console doctrine:schema:drop --force && bin/console doctrine:schema:create && bin/console doctrine:fixtures:load -n`

## Setup an admin user
`bin/console prpr:account:internal:create admin@bingham.works "Super Admin" testpass1234 ROLE_ACCESS_ADMIN`
`bin/console prpr:account:internal:create support@bingham.works "Customer Service" testpass1234 ROLE_ACCESS_CUSTOMER_SERVICE`

Or Individually  
1. Create a user
2. Then verify the user via the created uuid
3. Then change to the admin role 

### Run these commands
```
export UUID=$(bin/console ppl:account:internal:create admin@peopleprotect.works Admin test1234) && \
bin/console ppl:account:verify $UUID && \
bin/console ppl:account:change-site-access-role $UUID ROLE_ACCESS_ADMIN
``` 

## Tools

`vendor/bin/php-cs-fixer fix src`
`vendor/bin/phpstan analyse  -l 5 src`
`bin/console doctrine:schema:drop --force && bin/console doctrine:schema:create && bin/console doctrine:fixtures:load -n`
