export default [
    {
        path: '/appointment',
        name: 'appointment-list',
        meta: {
            title: 'Appointment'
        },
        // component: () => import(/* webpackChunkName: "team" */ '@/views/Team/TeamList'),
    },
    {
        path: '/appointment/create',
        name: 'appointment-create',
        meta: {
            title: 'Appointment - Create'
        },
        component: () => import(/* webpackChunkName: "team" */ '@/views/Appointment/CreateAppointment'),
    },
]