export default [
    {
        path: '/auth/login',
        name: 'auth-login',
        meta: {
            title: 'Login'
        },
        component: () => import(/* webpackChunkName: "auth" */ '@/views/Auth/Login')
    },
]