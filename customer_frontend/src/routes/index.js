import { createRouter, createWebHistory } from 'vue-router'
import Appointment from '@/routes/Appointment'
import Auth from '@/routes/Auth'
import Team from '@/routes/Team'

const routes = [
    ...Appointment,
    ...Auth,
    ...Team,
    {
        path: '/',
        name: 'dashboard',
        meta: {
            title: 'Dashboard'
        },
        component: () => import(/* webpackChunkName: "dashboard" */ '@/views/Dashboard')
    },
]

const router = createRouter({
    history: createWebHistory('/'),
    routes: routes,
})

export default router