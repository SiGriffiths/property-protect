import {createApp} from 'vue'
import App from '@/App.vue'
import vuetify from '@/plugins/vuetify'
import routes from '@/routes'
import {loadFonts} from '@/plugins/webfontloader'
import logOptions from '@/plugins/log'
import VueLogger from 'vuejs3-logger'

loadFonts()

createApp(App)
    .use(vuetify)
    .use(routes)
    .use(VueLogger, logOptions)
    .mount('#app')
    